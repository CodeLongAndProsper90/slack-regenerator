import os
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from py_dotenv import read_dotenv
import os
import random
import time
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
read_dotenv(dotenv_path)

client = WebClient(token=os.environ['TOKEN'])
while True:
    for i in range(1, 14):
        try:
            picdir = f"doctors/{i}"
            response = client.users_setPhoto(image=os.path.join(picdir, random.choice(os.listdir(picdir))))
            client.users_profile_set(status_text="Regenerating into Doctor #{i}")
        except SlackApiError as e:
            # You will get a SlackApiError if "ok" is False
            assert e.response["ok"] is False
            assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
            print(f"Got an error: {e.response['error']}")
        time.sleep(60*5)
