# Slack Regenerator
This is my personal slack profile changer
To use it, get your slack token and place it in .env like this:
```
TOKEN=your_token
```
and then run the changer with `nohup python3 main.py`
Oh almost forgot: install `py-dotenv` and `slack_sdk`
(Cropping the Doctors correctly is a next step)
